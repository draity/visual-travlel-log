const fs = require("fs");
const path = require("path");
const exiftool = require("exiftool-vendored").exiftool
const mustache = require("mustache");
const inquirer = require("inquirer");
const inquirerPath = require("inquirer-path");
const commandLineArgs = require('commander');
const rollup = require('rollup');
const _rollupConfig = require('./rollup.config')
const _routes = require('./src/routes')
 
const GOOGLE_DIRECTIONS_API_KEY = process.env.GOOGLE_DIRECTIONS_API_KEY
const dummyWaypoints = ["https://www.google.de/maps/dir/Reykjav%C3%ADk,+Island/Akureyri,+Island/@64.9330503,-21.617419,8z/data=!4m19!4m18!1m10!1m1!1s0x48d674b9eedcedc3:0xec912ca230d26071!2m2!1d-21.9426354!2d64.146582!3m4!1m2!1d-22.6001612!2d64.8423948!3s0x48d5a1a52d173ae3:0xef2a66a60a8df164!1m5!1m1!1s0x48d28f071cb0bfa7:0xbdb632798c71fdd1!2m2!1d-18.0906859!2d65.6825509!3e0", "https://www.google.de/maps/dir/Akureyri,+Island/H%C3%B6fn,+Island/@65.2889806,-15.9970634,8z/data=!4m19!4m18!1m10!1m1!1s0x48d28f071cb0bfa7:0xbdb632798c71fdd1!2m2!1d-18.0906859!2d65.6825509!3m4!1m2!1d-14.0273668!2d65.0626795!3s0x48cead1986919141:0x4ee67bd4413492f9!1m5!1m1!1s0x48cfac585a9c079f:0xb0ee81c829dbe0ed!2m2!1d-15.2020077!2d64.2497026!3e0"]
// [
//   "Reykjavik, Island",
//   "Borgarnes, Island",
//   "Akureyri, Island"
// ]

const imageFileExtensions = [ "jpg", "jpeg" ];
const filterFuncChoices = [
  { name: 'None', value: exifdata => true },
  { name: 'Rating >= 5', value: exifData => exifData.Rating >= 5 },
  { name: 'Rating >= 4', value: exifData => exifData.Rating >= 4 },
  { name: 'Rating >= 3', value: exifData => exifData.Rating >= 3 },
  { name: 'Rating >= 2', value: exifData => exifData.Rating >= 2 },
  { name: 'Rating >= 1', value: exifData => exifData.Rating >= 1 },
]
const template = fs.readFileSync('./template/index.mustache', 'utf-8');

const options = [
  { flag: 'imageDirectory <path>', short: 'i', message: 'Where are your images located?', default: process.cwd(), questionType: 'path' },
  { flag: 'title <title>', short: 't', message: 'What\'s the title of this travel log?', default: 'My travel log', questionType: 'input' },
  { flag: 'author <author>', short: 'a', message: 'Who\'s the author?', default: '', questionType: 'input' },
  { flag: 'description <description>', short: 'd', message: 'Add a description', default: '', questionType: 'input' },
  { flag: 'outputDirectory <path>', short: 'o', message: 'Where to save your travel log?', default: process.cwd(), questionType: 'path' },
  { flag: 'filterFunc <filterFuncName>', short: 'f', message: 'How do you want to filter the images?', default: 1, questionType: 'select-list' },
  { flag: 'routeItems [route-items]', short: 'r', message: 'What\'s your travel route? A list of waypoints (e.g. city names etc.) or Google Maps route urls (i.e. "https://www.google.de/maps/dir/...")', default: '', questionType: 'collect-list' },
]

commandLineArgs.version('0.1.0')
options.map(o => { // or reduce
  const parameters = [`-${o.short}, --${o.flag}`, o.message];
  if (o.questionType === 'select-list') {
    parameters.push(selectFilter);
  } else if (o.questionType === 'collect-list') {
    parameters.push(collectFilter);
  }
  commandLineArgs.option(...parameters);
});
commandLineArgs.parse(process.argv)

var questions = [
  {
    type: 'input',
    name: 'title',
    message: "What's the title of this travel log?",
    default: function() {
      return 'My travel log';
    },
    when: !commandLineArgs.title
  },
  {
    type: 'path',
    name: 'imageDirectory',
    message: 'Where are your images located?',
    default: process.cwd(),
    directoryOnly: true,
    when: !commandLineArgs.imageDirectory
  },
  {
    type: 'path',
    name: 'outputDirectory',
    message: 'Where to save your travel log?',
    default: process.cwd(),
    directoryOnly: true,
    when: !commandLineArgs.outputDirectory
  },
  {
    type: 'list',
    name: 'filterFunc',
    message: 'How do you want to filter the images?',
    choices: filterFuncChoices,
    default: 1,
    when: !commandLineArgs.filterFunc
  },
  {
    type: 'input',
    name: 'routeItems',
    message: 'What\'s the URL of your Google Maps route?',
    filter: function(val) {
      return [val];
    },
    when: function() {
      return GOOGLE_DIRECTIONS_API_KEY && !commandLineArgs.routeItems
    }
  }
];

(async () => {
  inquirer.registerPrompt('path', inquirerPath.PathPrompt);
  let answers = await inquirer.prompt(questions)
      answers = Object.assign(answers, commandLineArgs);

  console.log(`ℹ Reading files from: ${answers.imageDirectory}`);
  const imageFilePaths = readImagesFromDir(answers.imageDirectory, imageFileExtensions)
  
  const imageExifData$ = Promise.all(imageFilePaths.map(f => exiftool.read(f)))
  const routeGeojson$ = _routes.GetRouteGeojson(GOOGLE_DIRECTIONS_API_KEY, answers.routeItems || [])

  const [routeGeojson, imageExifData] = await Promise.all([routeGeojson$, imageExifData$])
  exiftool.end()

  const imageTemplateData = imageExifData.filter(answers.filterFunc).map(toImageTemplateData)
  const templateData = { 
    images: imageTemplateData,
    title: answers.title,
    author: answers.author,
    description: answers.description
  }  
  const html = mustache.render(template, templateData)

  let outputDir = await generateStandalonePresentation(answers, _rollupConfig, html, routeGeojson);
  console.log(`ℹ Wrote your Visual Travel Log to: '${outputDir}'`)
})();

// Helper functions

function toImageTemplateData(exifData) {
  return { 
      filepath: exifData.SourceFile, 
      fileId: escape(exifData.FileName),
      title: extractTitle(exifData),
      description: extractDescription(exifData), // caption abstract not yet supported in exiftool-vendored
      lat: exifData.GPSLatitude, 
      long: exifData.GPSLongitude,
      rotation: toClockwiseRotation(exifData.Orientation),
      mirrored: toMirrorDirection(exifData.Orientation)
    };
}

function extractTitle(exifData) {
  var description = exifData.Headline 
                 || exifData.ObjectName
                 || exifData.XPTitle
                //  || exifData.Subject;
  console.log(description)
  return description ? description.trim() : "";
}

function extractDescription(exifData) {
  var description = exifData.Description 
                 || exifData.ImageDescription 
                 || exifData.CaptionAbstract;
  return description ? description.trim() : "";
}

function toClockwiseRotation(exifOrientation) {
  // From exiftool documentation
  // 1 = Horizontal (normal)
  // 2 = Mirror horizontal
  // 3 = Rotate 180
  // 4 = Mirror vertical
  // 5 = Mirror horizontal and rotate 270 CW
  // 6 = Rotate 90 CW
  // 7 = Mirror horizontal and rotate 90 CW
  // 8 = Rotate 270 CW

  switch (exifOrientation) {
    case 1: return 0
    case 2: return 0
    case 3: return 180
    case 4: return 0
    case 5: return 270
    case 6: return 90
    case 7: return 90
    case 8: return 270
  }
}

function toMirrorDirection(exifOrientation) {
  // From exiftool documentation
  // 1 = Horizontal (normal)
  // 2 = Mirror horizontal
  // 3 = Rotate 180
  // 4 = Mirror vertical
  // 5 = Mirror horizontal and rotate 270 CW
  // 6 = Rotate 90 CW
  // 7 = Mirror horizontal and rotate 90 CW
  // 8 = Rotate 270 CW

  switch (exifOrientation) {
    case 2: return "horizontal"
    case 4: return "vertical"
    case 5: return "horizontal"
    case 7: return "horizontal"
  }
}

function readImagesFromDir(folderPath, imageFileExtensions) {
  return fs.readdirSync(folderPath, { withFileTypes: true })
    .filter(dirent => !dirent.isDirectory())
    .map(dirent => path.join(folderPath, dirent.name))
    .filter(fileName => imageFileExtensions.some(ext => fileName.endsWith(ext)));
  }

async function generateStandalonePresentation(config, rollupConfig, html, routePath){
  const outputDirectory = config.outputDirectory;
  const presentationTitle = config.title;
  const directoryName = presentationTitle.replace(/[^a-z0-9_\-]/gi, '_').toLowerCase();
  const directoryWithSubDir = path.join(outputDirectory, directoryName);

  if (!fs.existsSync(directoryWithSubDir)) {
    fs.mkdirSync(directoryWithSubDir)
  }
  
  await new Promise((resolve, reject) => fs.writeFile(path.join(directoryWithSubDir, "config.json"), JSON.stringify(config), err => err ? reject() : resolve()));
  copyRecursiveSync("./template", directoryWithSubDir);
  fs.writeFileSync(path.join(directoryWithSubDir, "assets", "route.js"), "var route = " + JSON.stringify(routePath));
  await Promise.all(rollupConfig.map(configEntry => rollupBundle(configEntry, path.join(directoryWithSubDir, "scripts"))));
  fs.writeFileSync(path.join(directoryWithSubDir, "index.html"), html);
  return directoryWithSubDir
}

var copyRecursiveSync = function(src, dest) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (exists && isDirectory) {
    if (!fs.existsSync(dest)) fs.mkdirSync(dest);
    fs.readdirSync(src).forEach(function(childItemName) {
      copyRecursiveSync(path.join(src, childItemName),
                        path.join(dest, childItemName));
    });
  } else {
    fs.copyFileSync(src, dest);
  }
};

async function rollupBundle(rollupConfigEntry, outputDirectory){
  var outputConfig = rollupConfigEntry.output;
  console.log(`Creating dependency bundle ${outputConfig.file}...`)
  
  // prepend output directory path
  outputConfig.file = path.join(outputDirectory, outputConfig.file);
  
  let bundle = await rollup.rollup(rollupConfigEntry)
  await bundle.write(outputConfig)
  console.log(`Bundle created: ${outputConfig.file}`);
  return outputConfig.file
}

function selectFilter(choice) {
  const filterFuncChoice = filterFuncChoices.find(c => c.name === choice);
  if (filterFuncChoice) {
    return filterFuncChoice.value;
  }

  return undefined;
}

function collectFilter(val, collected) {
  var collection = collected ? collected : []
  collection.push(val.trim());
  return collection;
}