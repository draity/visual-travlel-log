const fs = require("fs")
const https = require("https")
const polyline = require("@mapbox/polyline")
const GoogleURLData = require("./googleMapsParameterParser")

/**
 * Returns a geojson LineString from the given waypoints or Google Maps Directions URLs.
 * The first and the last waypoint of the last waypoint list are used as start and end. All waypoints inbetween are added in order.
 * @param {string} apiKey - The Google Directions API key, requested from: https://developers.google.com/maps/documentation/directions/start#get-a-key.
 * @param {string[]} waypointsOrUrls - List of waypoints. Either as place name (e.g. "Reykjavik, Island"), as Latitude/Longitude pair (e.g. "64.146582,-21.9426354"), or as unshortened Google Maps Directions Url, e.g. https://www.google.de/maps/dir/Reykjav%C3%ADk,+Island/Akureyri,+Island/@64.9330503,-21.617419,8z/data=!4m19!4m18!1m10!1m1!1s0x48d674b9eedcedc3:0xec912ca230d26071!2m2!1d-21.9426354!2d64.146582!3m4!1m2!1d-22.6001612!2d64.8423948!3s0x48d5a1a52d173ae3:0xef2a66a60a8df164!1m5!1m1!1s0x48d28f071cb0bfa7:0xbdb632798c71fdd1!2m2!1d-18.0906859!2d65.6825509!3e0.
 * @returns {Promise<{"type":"FeatureCollection", "features": {"type": string, "geometry": { "type": string, "coordinates": number[]}}[]}>} - geojson LineString
 */
function GetRouteGeojson(apiKey, waypointsOrUrls) {
    if (!apiKey) return Promise.resolve({"type":"FeatureCollection", "features":[]})  
    const waypoints = waypointsOrUrls.flatMap(wou => wou.startsWith("http") 
        ? ParseWaypoints(wou).map(w => `${w.lat},${w.lng}`)
        : [wou]
    )
    
    return new Promise((resolve, reject) => {
        const url = BuildApiUrl(apiKey, waypoints);
        // console.log(url)
        https.get(url, (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            });

            resp.on('end', () => {
                if (resp.statusCode < 200 || resp.statusCode > 299) {
                    reject(`Error: ${resp.statusCode}`)
                    return
                }
                let parsedData = JSON.parse(data)

                fs.writeFile(`route-${new Date(Date.now()).toISOString()}.json`, data, 'utf8', () => {});

                if (!parsedData.routes || parsedData.routes.length !== 1 || !parsedData.routes[0].overview_polyline){
                    reject(`Error: No polyline found in response data: ${data}`)
                    return
                }
                resolve(ConvertToGeoJson(parsedData.routes[0]));
            });

        }).on("error", (err) => {
            reject("Error: " + err.message);
        });
    });
}

/**
 * Returns waypoints from the given Google Maps Directions URL.
 * @param {string} url - Unshortened Google Maps Direction URL, e.g. https://www.google.de/maps/dir/Reykjav%C3%ADk,+Island/Akureyri,+Island/@64.9330503,-21.617419,8z/data=!4m19!4m18!1m10!1m1!1s0x48d674b9eedcedc3:0xec912ca230d26071!2m2!1d-21.9426354!2d64.146582!3m4!1m2!1d-22.6001612!2d64.8423948!3s0x48d5a1a52d173ae3:0xef2a66a60a8df164!1m5!1m1!1s0x48d28f071cb0bfa7:0xbdb632798c71fdd1!2m2!1d-18.0906859!2d65.6825509!3e0.
 * @returns {{lat: string, lng: string, primary: boolean}[]} LatLong array of waypoints
 */
function ParseWaypoints(url){
    const urlData = new GoogleURLData(url)
    return urlData.getRoute().getAllWaypoints()
}

/**
 * Converts the given polyline to a Geojson LineString
 * @param {{overview_polyline: {points: string}, legs: {start_location: {lat: Number, lng: Number}, end_location: {lat: Number, lng: Number}}[]}} route - polyline as returned by the Google Maps Direction API
 * @returns {{type: 'LineString', coordinates: [number, number]}} geojson LineString
 */
function ConvertToGeoJson(route){    
    const routeLine = polyline.toGeoJSON(route.overview_polyline.points);
    const routeLineFeature = {"type":"Feature","geometry": routeLine};

    const points = route.legs.flatMap(l => [
        {location: l.start_location, address: l.start_address},
        {location: l.end_location, address: l.end_address}
    ])
    const distinctPoints = points.reduce(
        (distinct, point) => !distinct.some(others => point.location.lat === others.location.lat && point.location.lng === others.location.lng) ? [...distinct, point] : distinct,
        []
    );
    const pointFeatures = distinctPoints.map(p => ({
        "type":"Feature",
        "geometry": {"type":"Point","coordinates":[p.location.lng, p.location.lat]},
        "properties": {"name": p.address}
    }));

    return {"type":"FeatureCollection","features":[ routeLineFeature ].concat(pointFeatures)}
}

/**
 * Builds a Google Maps Directions request URL based on given waypoints.
 * @param {string} apiKey - The Google Directions API key, requested from: https://developers.google.com/maps/documentation/directions/start#get-a-key.
 * @param {string[]} waypoints - List of waypoints. Either as place name (e.g. "Reykjavik, Island") or as Latitude/Longitude pair (e.g. "64.146582,-21.9426354")
 * @returns {string} - Google Maps Direction API request URL
 */
function BuildApiUrl(apiKey, waypoints){
    let apiUrl = "https://maps.googleapis.com/maps/api/directions/json?"
    // apiUrl = "http://localhost:8080/?"
    
    const encodedWaypoints = waypoints.map(w => encodeURIComponent(w))
    
    const origin = encodedWaypoints.shift()
    const destination = encodedWaypoints.pop()

    if (origin && destination) {
        apiUrl += `origin=${origin}`
        apiUrl += `&destination=${destination}`
        encodedWaypoints.forEach((waypoint, i) => {
            if (i === 0) {
                apiUrl += `&waypoints=${waypoint}`
            } else {
                apiUrl += `|${waypoint}`
            }
        });
        apiUrl += `&key=${apiKey}`
        return apiUrl
    } else {
        return undefined
    }
}

module.exports = {
    GetRouteGeojson: GetRouteGeojson
}