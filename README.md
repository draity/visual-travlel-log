# Visual Travel Log

Visualize your trips around the world based on geotagged images.

## Motivation

_Travel picture presentations are nice mementoes._

__But why are they sooo boring!__ And why is it so hard follow longer roadtrips?

Ever showed some travel pictures to friends or family only to open Google Maps afterwards to show the actual geographical points you visited?

If so, this project is for you!

A __presentation generation tool__ to overlay a simple SVG map with geotagged pictures which can be presented one after another following your travel route.

Enjoy!

## How it works

The __source of truth__ for this travel log presentation generator __are your images__.
All required information is extracted from metadata using [exiftool](https://www.sno.phy.queensu.ca/~phil/exiftool/):

* Geotags to map the images to their location
* Image title to set an optional image caption (based on tags: Headline, ObjectName, XPTitle or Subject)
* Image description for additional information (based on tags: Description, ImageDescription)
* Image orientation

This extraction is done once for each presentation during generation (if you want to know why, check the [Design decisions](#design-decisions) below). The presentation is generated based on all (or some of) the .jpg/.jpeg files in the image input directory by using the `template` and the `generate.js` script. After this step your freely chosen output directory contains a static website mainly based on [impress.js](https://github.com/impress/impress.js) and [D3](https://d3js.org/)

## Usage

* [Install](https://nodejs.org/en/download/package-manager/) `node` if not already available
* Install dependencies
    `npm install`
* Generate your presentation:
    `npm run generate` or `node generate.js`

    ![Generate travel log](./doc/generate_travel_log.gif)

    If you don't like all these quesions, feel free to use the command line arguments:

    ```bash
    node generate.js -i "/temp/example" -t "another title" -o "/temp/example2/" -a "Author Name" -d "An optional description of your journey"
    ```

    You could even store your fixed arguments in another npm script by adding e.g.:
    ```json
    "scripts": {
        "generate": "node generate.js",
        "generateForMe": "node generate.js -a 'Your Name'"
    },
    ```

    If you want to add a travel route based on Google Maps you have to have an API key and a registered credit card in the Google Cloud Platform Console, see https://developers.google.com/maps/documentation/directions/start#get-a-key. 
    At the time of writing you get 200$ of free credit to use the APIs, which is equivalent to 20000 free requests (with more than 10 waypoints). 
    For up-to-date billing informations check [the billing page](https://developers.google.com/maps/documentation/directions/usage-and-billing) or the [Pricing Calculator](https://mapsplatformtransition.withgoogle.com/calculator).
    If you have an activated API key set it as environment variable `GOOGLE_DIRECTIONS_API_KEY` before calling the `generate.js` script (e.g. using `GOOGLE_DIRECTIONS_API_KEY=<key> node generate.js` or `export GOOGLE_DIRECTIONS_API_KEY=<key>`), you will be asked to provide an unshortened Google Maps Directions URL. Alternatively use as many `-r` command line arguments as you like to provide multiple waypoints as place names ('Reykjavik, Island') or coordinates ('64.146582,-21.9426354'), or/and multiple unshortened Google Maps Directions URLs. Order is important, for example:

    ```bash
    node generate.js -r 'Reykjavik, Island' -r 'https://www.google.de/maps/dir/Akureyri,+Island/H%C3%B6fn,+Island/@65.2889806,-15.9970634,8z/data=!4m19!4m18!1m10!1m1!1s0x48d28f071cb0bfa7:0xbdb632798c71fdd1!2m2!1d-18.0906859!2d65.6825509!3m4!1m2!1d-14.0273668!2d65.0626795!3s0x48cead1986919141:0x4ee67bd4413492f9!1m5!1m1!1s0x48cfac585a9c079f:0xb0ee81c829dbe0ed!2m2!1d-15.2020077!2d64.2497026!3e0' -r '63.416202,-19.010384'
    ```

    results in a route from Reykjavik to Akureyri to Höfn to '63.416202,-19.010384' (Vík í Mýrdal) in that order.

* Open the `index.html` file in the output directory
* Enjoy

    ![Example travel log](./doc/travel_log.gif)

* If not, you can adapt the presentation however you like by:
    * Editing `index.html` to e.g. add custom steps, remove images, change image captions
    * Editing CSS files to personalize appearence, e.g. size of images, fonts, colors
    * Editing JS output files in `scripts` to change mapping and impress.js behaviours, e.g. change map alignment, 
    * Replacing/editing assets to e.g. change the map or fonts
    * If you need access to exif data stored in your images files, e.g. to render further information or want to make some changes for all your travel logs: edit `generate.js` and edit files in `template`

## Design decisions

* Use standard web tools for the generated presentations
    * Reasons: 
        * Browsers are designed for backwards compatibility, 
        * APIs are standardized
        * Browsers are installed everywhere
    * Solution: combine `D3` and `impress.js`
* Offline usage of the generated presentation should be possible
    * Reasons: e.g. presenting at a relative's home without wifi access
    * Consequence: no external script/font loading (i.e. CDN)
    * Solution: Install packages locally via npm and use Rollup during generation to build a local vendor bundle
* Self-contained: No need to install anything to run the presentation
    * Reasons: Simplicity and ease of use, just double click the html file
    * Consequences: 
        * no build tool for the generated presentation, just plain old html+js+css
        * no dev server, which means no AJAX call to local resources
* Simple customization of presentations should be possible. 
    * Reasons:
        * Add further slides not based on images
        * Change image locations manually to avoid overlapping
        * Use your own styling preferences
        * ...
    * Consequence: Output should be clear, no minified JS
    * Solution: use a generator with sane defaults to handle e.g. Exif data rendering to a template but allow overwriting every aspect of the result manually (i.e. via HTML, JS, CSS).
* Generator should be easy to use.
    * Reason: prevent forgetting important information and manual actions
    * Solution: Use a CLI script which is configurable via cmd options and ask for additional/missing input if neccessary

## Notes & Credits

* Maps from: https://github.com/simonepri/geo-maps/blob/master/info/countries-coastline.md#countries-coastline
* Font [Traveling Typewriter](https://www.dafont.com/de/traveling-typewriter.font) by Carl Krull (Free for personal use, please contact the author if you use the font for commercial purposes: http://carlkrull.dk/contact/)
* [googleMapsParameterParser](./src/googleMapsParameterParser.js) (c) 2016 David Edgar, see https://github.com/david-r-edgar/google-maps-data-parameter-parser

## Further ideas

* Planned: see [TODOs](./TODO.md)
* Automatic clustering for locations with multiple images
* Retrieve location information, e.g. City names for clusters