# TODOs

* Replace large SVG, as it renders quite slowly and breaks zooming without the current hack of duplicating the svg item
  * Use d3-tile to render map tiles
    * Fast
    * Possibility to increase details level when zoomed in
    * Source of tiles? Should be offline, else we have a dependency to e.g. mapbox
    * We'd have to read the scale of the next slide and load new tiles for that scale in the correct location
    * https://observablehq.com/@d3/zoomable-raster-vector?collection=@d3/d3-tile
  * Use Canvas
    * May be more performant
    * Pixelated without recalculating for each scale at the correct location
    * Lib for SVG to Canvas: https://canvg.github.io/canvg/index.html
    * Example:

        ```
        window.onload = () => {

          const canvas = document.querySelector('canvas');
          const ctx = canvas.getContext('2d');
    
          v = canvg.Canvg.fromString(ctx, 
            '<svg xmlns="http://www.w3.org/2000/svg" id="map-step">....</svg>', 
            {scaleWidth: 182000, offsetX: -135, offsetY: -330});
          )
          v.resize(1000,1000)

          // Start SVG rendering with animations and mouse handling.
          v.start();
        };
        ```
    * Use offset and scaleWidth to focues on the correct location + manual placement
    * Use Canvas only after zooming in: https://jakearchibald.com/2017/lazy-async-svg/

* Write and read config file
* Fix overlapping images for going back (fowards works as expected)
* Simplifiy configuration of generated presentation (color etc., e.g. with css variables?)
* Add easy possibility to add sections (maybe use keywords? but how exactly? namespaced? or allow to specify the keywords<->section mapping?)
* Add last slide with all next-location photos
* Replacement of corner figcaptions for portrait images 
* Add possibility to label waypoints (automatically or manually? placement?)