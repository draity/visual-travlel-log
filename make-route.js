const fs = require("fs");
const path = require("path");
const exiftool = require("exiftool-vendored").exiftool
const mustache = require("mustache");
const inquirer = require("inquirer");
const inquirerPath = require("inquirer-path");
const commandLineArgs = require('commander');
const rollup = require('rollup');
const _rollupConfig = require('./rollup.config')
const _routes = require('./src/routes')
 
const GOOGLE_DIRECTIONS_API_KEY = process.env.GOOGLE_DIRECTIONS_API_KEY
const dummyWaypoints = ["https://www.google.de/maps/dir/Reykjav%C3%ADk,+Island/Akureyri,+Island/@64.9330503,-21.617419,8z/data=!4m19!4m18!1m10!1m1!1s0x48d674b9eedcedc3:0xec912ca230d26071!2m2!1d-21.9426354!2d64.146582!3m4!1m2!1d-22.6001612!2d64.8423948!3s0x48d5a1a52d173ae3:0xef2a66a60a8df164!1m5!1m1!1s0x48d28f071cb0bfa7:0xbdb632798c71fdd1!2m2!1d-18.0906859!2d65.6825509!3e0", "https://www.google.de/maps/dir/Akureyri,+Island/H%C3%B6fn,+Island/@65.2889806,-15.9970634,8z/data=!4m19!4m18!1m10!1m1!1s0x48d28f071cb0bfa7:0xbdb632798c71fdd1!2m2!1d-18.0906859!2d65.6825509!3m4!1m2!1d-14.0273668!2d65.0626795!3s0x48cead1986919141:0x4ee67bd4413492f9!1m5!1m1!1s0x48cfac585a9c079f:0xb0ee81c829dbe0ed!2m2!1d-15.2020077!2d64.2497026!3e0"]
// [
//   "Reykjavik, Island",
//   "Borgarnes, Island",
//   "Akureyri, Island"
// ]

const options = [
  { flag: 'routeItems [route-items]', short: 'r', message: 'What\'s your travel route? A list of waypoints (e.g. city names etc.) or Google Maps route urls (i.e. "https://www.google.de/maps/dir/...")', default: '', questionType: 'collect-list' },
]

commandLineArgs.version('0.1.0')
options.map(o => { // or reduce
  const parameters = [`-${o.short}, --${o.flag}`, o.message];
  if (o.questionType === 'collect-list') {
    parameters.push(collectFilter);
  }
  commandLineArgs.option(...parameters);
});
commandLineArgs.parse(process.argv)

var questions = [
  {
    type: 'input',
    name: 'routeItems',
    message: 'What\'s the URL of your Google Maps route?',
    filter: function(val) {
      return [val];
    },
    when: function() {
      return GOOGLE_DIRECTIONS_API_KEY && !commandLineArgs.routeItems
    }
  }
];

(async () => {
  inquirer.registerPrompt('path', inquirerPath.PathPrompt);
  let answers = await inquirer.prompt(questions)
      answers = Object.assign(answers, commandLineArgs);

  const routeGeojson = await _routes.GetRouteGeojson(GOOGLE_DIRECTIONS_API_KEY, answers.routeItems || [])
  exiftool.end()

  fs.writeFileSync(`geojson-${new Date(Date.now()).toISOString()}.json`, "var route = " + JSON.stringify(routeGeojson));

  console.log(`ℹ Generated route'`)
})();

// Helper functions

function collectFilter(val, collected) {
  var collection = collected ? collected : []
  collection.push(val.trim());
  return collection;
}