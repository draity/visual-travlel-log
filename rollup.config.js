const resolve = require('rollup-plugin-node-resolve')
const commonjs = require('rollup-plugin-commonjs')

module.exports = [{
    input: 'template/vendor.js',
    output: {
        // use window to make dependencies available as globals to standard scripts
        name: 'window',
        file: 'vendor.js',
        format: 'umd',
        // extend window instead of replacing it
        extend: true 
    },
    plugins: [
        resolve(),
        // Since most packages in the node_modules folder are probably legacy CommonJS rather than JavaScript modules
        commonjs() 
    ],
    onwarn: function ( message ) {
        if (message.code === 'CIRCULAR_DEPENDENCY') {
            return;
        }
        console.error(message);
    }
}];