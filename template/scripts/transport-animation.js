(function(document) {
  document.addEventListener('impress:stepenter', function(e){
    // setTimeout(triggerFlightAnimation, 1000)

    var flightSlide = document.querySelector( ".active[data-flight]" );
    if (flightSlide) {
      console.log("register transitionend for flight")
      const impressSubElement = document.querySelector("#impress>div")
      let lastValue = undefined
      let intervalId = setInterval(() => {
        const currentValue = getComputedStyle(impressSubElement).transform
        if (lastValue === currentValue) {
          clearInterval(intervalId);
          triggerFlightAnimation(flightSlide)
        }
        lastValue = currentValue
      }, 500)
    }
  });

  document.addEventListener("keydown", (e) => {
		if(e.code === "KeyF") {
			var flightSlide = document.querySelector( ".active[data-flight]" );
      if (flightSlide) {
        triggerFlightAnimation(flightSlide)
      }
		}
	})

  function triggerFlightAnimation(flightSlide) {
      const flightPath = document.querySelector(`path#flight-to-${flightSlide.dataset.flight}`)
      animateFlight(flightPath, translateAlong, 0, 0, 40, flightSlide.dataset.scale)
  }

  // Animate an font-awesome plane along the first path.datamaps-arc using
  // translateAlong as the tween transform function
  // offsetX and offsetY as offset to center the plane on the path
  // rotation defines a fixed one-time rotation of the animated object
  function animateFlight(flightPath, translateAlong, offsetX, offsetY, rotation, scale){
    var svg = d3.select(".svg-map");

    if (!flightPath) {
      return
    }

    startPoint = flightPath.getPointAtLength(0)  //map.latLngToXY(startLat, startLng)

    var marker = svg.append('text')
        .attr('font-family', 'FontAwesome')
        .attr('font-size', '18px')
        .attr('id', 'plane')
        .style("transform-origin", "50% 50%")
        .style("transform-box", "fill-box") // behave like a regular HTML element where 50% is relative to the element itself
        .style('fill', 'black')
        .text('\uf072')
        .attr("transform", 'translate(' + [startPoint.x + offsetX, startPoint.y + offsetY] + ')'); 

    transition();

    function transition() {
      const l = flightPath.getTotalLength()  
      marker.transition()
            .duration(Math.sqrt(l) * 250)
            .attrTween("transform", translateAlong(flightPath, offsetX, offsetY, rotation) )
            .attrTween("font-size", translateFontAlong(flightPath, scale))
            .remove();
    }
  }

  // define a reversed parabel to scale the plane size along the path to simulate flight height
  function translateFontAlong(path, scale) {
    var l = path.getTotalLength();
    return function(i) {
    return function(t) {
        return (-200*(t-0.5)*(t-0.5) + 68) * scale + "px";//Move marker
    }
    }
  }

  function translateAlong(path, offsetX, offsetY, rotationOffset, reverse) {
    let direction = reverse ? -1 : 1
    var l = path.getTotalLength();
    return function(d, i, a) {
      return function(t) {
        const atLength = direction === 1 ? (t * l) : (l - (t * l));
        var p1 = path.getPointAtLength(atLength),
            p2 = path.getPointAtLength((atLength)+direction),
            angle = Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
        return "translate(" + (p1.x + offsetX) + "," + (p1.y + offsetY) + ")rotate(" + (angle + rotationOffset) + ")";
      }
    }
  }
})(document)