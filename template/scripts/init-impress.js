(function (document, impress) {
    var impressAPI = impress();
    impressAPI.init("impress");

    var viewportWidth = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
    var viewportHeight = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;

    //
    // Helper functions
    //

    var distanceInRange = function(x1, x2, maxDistance) {
        return  Math.abs(x1 - x2) < maxDistance
    }

    var slidesDirectionOverlaps = function(currentSlide, nextSlide, directionAttribute, directionMaxDistance) {
        return distanceInRange(
            currentSlide.getAttribute(directionAttribute),
            nextSlide.getAttribute(directionAttribute),
            directionMaxDistance
        ) 
    }

    var slidesOverlap = function(viewportWidth, viewportHeight) {
        return function(current, next) {
            return slidesDirectionOverlaps(current, next, 'data-x', viewportWidth * next.getAttribute('data-scale')) 
                || slidesDirectionOverlaps(current, next, 'data-y', viewportHeight * next.getAttribute('data-scale'));
        }
    }(viewportWidth, viewportHeight)

    //
    // Register custom impress event handlers
    //

    // If we move to a new location, add the "previous-location" class to the figure which keeps the image displayed for visual reference
    document.addEventListener('impress:stepleave', function(e){
        var current = event.target;
        var next = document.querySelector(".active");
        if (next != null && !slidesOverlap(current, next))
        {
            current.classList.add("previous-location")
        }
    });

    // add "first" class to slides which define a new position (e.g. not moved there before)
    // these slides will be displayed one step earlier with reduced opacity and visaully guide the movement
    document.addEventListener('impress:stepenter', function(e) {
        var current = document.querySelector( ".active" );
        var next = document.querySelector(".active + .step");
        if (next != null){
            var pastSlideAtSamePositionAsNext = document.querySelector(".past[data-x='" + next.getAttribute("data-x") + "'][data-y='" + next.getAttribute("data-y") + "']")
            if ( pastSlideAtSamePositionAsNext == null // no other slide was here before
                && !slidesOverlap(current, next)) { // the current slide is elsewhere
                next.classList.add("next-location")
            }
        }
    })

    // if there are images which were not loaded before entering this step
    // (e.g. because of direct link or moving too fast)
    // load the images now
    document.addEventListener('impress:stepenter', function(e){
        var hiddenImagesInCurrentStep = document.querySelectorAll( ".active > img:not([src])")
        if (hiddenImagesInCurrentStep != null) {
            hiddenImagesInCurrentStep.forEach(function (nextImg) {
                var dataSrc = nextImg.getAttribute("data-src");
                if (dataSrc != null && dataSrc.length > 0) {
                    nextImg.setAttribute('src',dataSrc);
                }
            })
        }

    });

    // find the next images and pre load them
    // allows smooth transitions
    document.addEventListener('impress:stepenter', function(e){
        var nextImgs = document.querySelectorAll( ".active + .step > img[data-src]" );
        if (nextImgs != null){
            nextImgs.forEach(function (nextImg) {
                var dataSrc = nextImg.getAttribute("data-src");
                if (dataSrc != null && dataSrc.length > 0) {
                    nextImg.setAttribute('src',dataSrc);
                }
            })
        }
    });

    // remove all images by removing the src attribute when
    // moving to the next section
    // exception: keep the last visible slide for visual reference
    document.addEventListener('impress:stepleave', function(e){
        var isNextADiv = document.querySelector( "section.active" ) != null;
        if (isNextADiv) {
            var pastSlides = Array.prototype.slice.call(document.querySelectorAll( ".past" ));
            if (pastSlides != null && pastSlides.length > 0){
                pastSlides.pop().classList.add("previous-location") // keep the last visible slide
                pastSlides.forEach(function (pastImg) {
                        pastImg.querySelectorAll("img").forEach(function (img) {img.removeAttribute("src")});
                })
            }
        }
    });
})(document, impress);