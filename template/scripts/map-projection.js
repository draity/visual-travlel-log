(function (document, d3, turf, geojson, route) {
    var mapElement = document.getElementById("map-step");
    var mapWidthHeight = getWidthAndHeight(mapElement);
    setImpressPositionCentered(mapElement, mapWidthHeight);

    var additionalCountries = [] //['ESP']
    var imagePoints = Array
        .from(document.getElementsByClassName("photo-view"))
        .map(element => extractGeoCoords(element));
    geojson.features = geojson.features
        .filter(geoFeature => geoFeature && geoFeature.geometry && geoFeature.geometry.type)
        .filter(countryPolygon => isAnyImageInside(imagePoints, countryPolygon) || (countryPolygon.properties && countryPolygon.properties.A3 && additionalCountries.includes(countryPolygon.properties.A3)))
    routeLineFeature = route.features.filter(f => f.geometry.type === "LineString")
    carRouteFeatures = routeLineFeature.filter(f => !f.properties || f.properties.travelMode !== "flight")
    flightRouteFeatures = routeLineFeature.filter(f => f.properties && f.properties.travelMode === "flight")


    var projection = d3.geoMercator().fitSize(mapWidthHeight, geojson)
        // OPTIONAL recenter, scale or translate the map using:
        // .center([-7, 1])
        // .scale(100)
        // .translate([document.getElementById("impress").offsetWidth / 2, document.getElementById("impress").offsetHeight / 2])

    var geoGenerator = d3.geoPath()
        .projection(projection);
    var projectionScale = projection.scale()

    d3.select('.map')
        .selectAll('path')
        .data(geojson.features)
        .enter()
        .append('path')
        .attr('d', geoGenerator)
        .style('stroke-width', projectionScale * 0.0003)
        .attr('data-country', function(d) {
            return d.properties.A3
        })
        .attr('class', function(d) {
            // return ['TZA', 'DEU'].includes(d.properties.A3) ? "visited" : "rest";
            return isAnyImageInside(imagePoints, d) ? "visited" : "rest";
        });

    d3.select('.map')
        .selectAll('.label')
        .data(geojson.features)
        .enter()
        .append('text')
        .attr("x", function(d) {
            return geoGenerator.centroid(d)[0];
        })
        .attr("y", function(d) {
            return geoGenerator.centroid(d)[1];
        })
        .attr("text-anchor", "middle")
        .attr("font-size", function(d) { return Math.sqrt(geoGenerator.area(d)) * 0.1 + 'px' })
        .attr("fill", "black")
        .text(function(d) {
            // country codes, see e.g. https://unstats.un.org/unsd/tradekb/knowledgebase/country-code
            return d.properties.A3
        })

    d3.select('.map')
        .selectAll('.route')
        .data(carRouteFeatures)
        .enter()
        .append('path')
            .attr('d', geoGenerator)
            .style('stroke-width', projectionScale * 0.0001)
            .style('stroke', function(d) { return d.geometry.color || 'red' })
            .style('fill', 'none');

    d3.select('.map')
        .selectAll('.flights')
        .data(flightRouteFeatures)
        .enter()
        .append('path')
            .attr("d", arcGenerator)
            .attr("id", function(d) { return d.properties.id ? `flight-to-${d.properties.id}` : null; })
            .attr('class', 'flight-route')
            .style('stroke-width', projectionScale * 0.0005)
            .style('stroke', function(d) { return d.properties.color || 'black' })
            .style('stroke-opacity', function(d) { return d.properties.opacity || 0.5 })
            .style('fill', 'none');
    
    const waypoints = d3.select('.map')
        .selectAll('.waypoint')
        .data(route.features.filter(f => f.geometry.type === "Point"))
        .enter()
        waypoints.append('circle')
            .attr("cx", function (d) { return projection(d.geometry.coordinates)[0]; })
            .attr("cy", function (d) { return projection(d.geometry.coordinates)[1]; })
            .attr("r", projectionScale * 0.0003)
            .attr("title", function (d) { return d.properties.name; })
            .style("fill", function(d) { return d.properties.color || 'black' })
            .style('stroke', 'none')
        waypoints.filter(d => d.properties.showText).append("text")
            .attr("x", function (d) { return projection([d.geometry.coordinates[0]+ (d.properties?.textOffset?.[0] || 0), d.geometry.coordinates[1]+ (d.properties?.textOffset?.[1] || 0)])[0]; })
            .attr("y", function (d) { return projection([d.geometry.coordinates[0]+ (d.properties?.textOffset?.[0] || 0), d.geometry.coordinates[1]+ (d.properties?.textOffset?.[1] || 0)])[1]; })
            .style("font-size", "0.004em")
            .style("fill", "white")
        .text(function (d) { return d.properties.name; })

    var mapScale = mapElement.getAttribute("data-scale") || 1
    d3.selectAll('[data-lat]').each(
        function () {
            var img = d3.select(this)
            var lat = img.attr('data-lat')
            var long = img.attr('data-long')
            var imgProjection = projection([long, lat])
            img.attr('data-x', imgProjection[0] * mapScale - (mapScale - 1) * mapWidthHeight[0] / 2)
            img.attr('data-y', imgProjection[1] * mapScale - (mapScale - 1) * mapWidthHeight[1] / 2)
            img.attr('data-scale', img.attr('data-scale') * projectionScale * 0.001)
        }
    );

    function getWidthAndHeight(mapElement) {
        var rect = mapElement.getBoundingClientRect()
        var w = rect.width
        var h = rect.height

        return [w, h]
    }

    function setImpressPositionCentered(element, [w, h]) {
        element.setAttribute("data-x", w / 2)
        element.setAttribute("data-y", h / 2)
    }

    function extractGeoCoords(element) {
        return [
            parseFloat(element.getAttribute("data-long")),
            parseFloat(element.getAttribute("data-lat"))
        ]
    }

    function isAnyImageInside(imagePoints, geometry) {
        return imagePoints.some(pt => turf.booleanPointInPolygon(pt, geometry))
    }

    function arcGenerator(feature) {
        let originXY = projection(feature.geometry.coordinates[0])
        let destXY = projection(feature.geometry.coordinates[1])
        let midXY = [ (originXY[0] + destXY[0]) / 2, (originXY[1] + destXY[1]) / 2]
        let arcSharpness = feature.properties.arcSharpness || 1
        return "M" + originXY[0] + ',' + originXY[1] + "S" + (midXY[0] + (50 * arcSharpness)) + "," + (midXY[1] - (75 * arcSharpness)) + "," + destXY[0] + "," + destXY[1];
    }
})(document, d3, turf, geojson, route);