// redraw SVG hack:
// because of transform scale by impress the svg will get blurry for large scale factors.
// until now I couldn't find any reasonable method to avoid it
// so to get a sharp svg we use the following hack of reattaching if scale factors change between slides
(function (document) {
	var parseTransform = function (transformString) {
		var prop = ['translate', 'matrix', 'rotate', 'skewX', 'skewY', 'scale'];
		var val = transformString.match(/(translate|matrix|rotate|skewX|skewY|scale)\(.*?\)/g);
		var obj = {};
		if (val) {
			for (var i = 0, length = val.length; i < length; i++) {
				var item = val[i];
				var index = item.indexOf('(');
				var v = item.substring(index + 1, item.length - 1).split(/\,|\s/);
				var n = item.substring(0, index);
				obj[n] = {};
				switch (n) {
					case 'translate':
					case 'scale':
						obj[n].x = +v[0] || 0;
						obj[n].y = +v[1] || 0;
						break;
					case 'rotate':
						obj[n].a = +v[0] || 0;
						obj[n].x = +v[1] || 0;
						obj[n].y = +v[2] || 0;
						break;
					case 'skewX':
					case 'skewY':
						obj[n].a = +v[0];
						break;
					case 'matrix':
						obj[n].a = +v[0] || 0;
						obj[n].b = +v[1] || 0;
						obj[n].c = +v[2] || 0;
						obj[n].d = +v[3] || 0;
						obj[n].e = +v[4] || 0;
						obj[n].f = +v[5] || 0;
						break;
				}
			}
		}

		obj.toString = function () {
			var builder = [];
			for (var i = 0, length = prop.length; i < length; i++) {
				var n = prop[i];
				var o = this[n];
				if (!o)
					continue;
				switch (n) {
					case 'translate':
					case 'scale':
						builder.push(n + '(' + o.x + ',' + o.y + ')');
						break;
					case 'rotate':
						builder.push(n + '(' + o.a + ' ' + o.x + ' ' + o.y + ')');
						break;
					case 'skewX':
					case 'skewY':
						builder.push(n + '(' + o.a + ')');
						break;
					case 'matrix':
						builder.push(n + '(' + o.a + ',' + o.b + ',' + o.c + ',' + o.d + ',' + o.e + ',' + o.f + ')');
						break;
				}
			}
			return builder.join(' ');
		};

		return obj;
	};


	let scale = undefined
	let newScale = undefined
	let finished = 0
	const impressRootElement = document.querySelector("#impress")
	const impressSubElement = document.querySelector("#impress>div")
	impressRootElement.addEventListener("transitionstart", (e) => {
		if ((e.target != impressRootElement && e.target != impressSubElement) || e.propertyName !== "transform") {
			return;
		}

		console.log("transitionstart - reset finished")
		finished = 0
		newScale = undefined
		if (e.target == impressRootElement) {
			scale = parseTransform(getComputedStyle(e.target).transform).matrix.a

			let zoomed = document.getElementById("map-step-zoomed")
			if (zoomed) {
				document.getElementById("map-step").style.display = "initial"
				setTimeout(() => zoomed.parentElement.removeChild(zoomed), 500)
				
			}
		}
	})

	impressRootElement.addEventListener("transitionend", (e) => {
		if ((e.target != impressRootElement && e.target != impressSubElement) || e.propertyName !== "transform") {
			return;
		}

		if (finished === 2) {
			console.warn("already finished 2 transition but called again")
			finished = 0
			scale = undefined
			newScale = undefined
		}
		
		console.log("transitionend - finished++")
		finished += 1

		if (e.target === impressRootElement) {
			newScale = parseTransform(getComputedStyle(e.target).transform).matrix.a
		}
		console.log("rerender map scales: ", scale, newScale)
		if (finished === 2 && Math.abs(scale - newScale) > 0.1) {
			console.log("transitionend - reset finished")
			finished = 0
			scale = undefined
			newScale = undefined
			// setTimeout(() => {
				console.log("rerender map")
				const orig = document.getElementById('map-step')
				const x = orig.cloneNode(true)
				x.id = "map-step-zoomed"
				impressSubElement.insertBefore(x, impressRootElement.querySelector(".step"))
				orig.style.display = "none"
				// impressSubElement.removeChild(orig)
			// }, 500)
		}
	})

	document.addEventListener("keydown", (e) => {
		if(e.code === "KeyR") {
			console.log("rerender map, manually triggered")
			const orig = document.getElementById('map-step')
			const x = orig.cloneNode(true)
			x.id = "map-step-zoomed"
			x.style.display = "initial"
			const zoomed = document.getElementById('map-step-zoomed')
			if (zoomed) {
				impressSubElement.removeChild(zoomed)
			}
			impressSubElement.insertBefore(x, impressRootElement.querySelector("#map-step"))
		}
	})
})(document)
