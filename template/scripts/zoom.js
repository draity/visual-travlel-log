function toggleZoom(e) {
    e.preventDefault();
    e.stopPropagation();
    let figure = e.target.parentElement;
    let img = e.target;
    
    if (!figure.classList.contains("zoom")) {
        // add image as figure background image
        figure.classList.add(...[...img.classList].filter(c => c.startsWith("rotate")))
        let zoomView = document.createElement("div")
        zoomView.classList.add("zoom-view")
        zoomView.style.backgroundImage = `url('${e.target.dataset.src}')`
        figure.appendChild(zoomView)
        // figure.style.setProperty('--zoom-size', '200%')
        // figure.style.setProperty('--zoom-position', "50% 50%")
        // figure.style.setProperty('--zoom-image', `url('${e.target.dataset.src}')`)
    } else {
        // remove if not needed to avoid lots of doubly loaded images
        // figure.style.removeProperty("background-image");
        figure.removeChild(figure.querySelector(".zoom-view"))
    }
    
    figure.classList.toggle("zoom")
    
}

function zoom(e){
    let zoomer = e.currentTarget;
    
    if (!zoomer.classList.contains("zoom")) return;
    
    let offsetX = e.offsetX ? e.offsetX : e.touches[0].pageX
    let offsetY = e.offsetY ? e.offsetY : e.touches[0].pageX
    let x = offsetX/zoomer.offsetWidth*100
    let y = offsetY/zoomer.offsetHeight*100
    
    // zoomer.style.backgroundPosition = x + '% ' + y + '%';
    zoomer.querySelector(".zoom-view").style.backgroundPosition = x + '% ' + y + '%';
    // zoomer.style.setProperty('--zoom-position', x + '% ' + y + '%')
}

function magnify(e){
    e.preventDefault();

    let config = {
        default: 200,
        min: 100,
        max: 1000,
        step: 10,
    }
    let deltaY = e.deltaY;
    let zoomer = e.target.parentElement;

    if (!zoomer.classList.contains("zoom")) return;
    let zoomView = zoomer.querySelector(".zoom-view")

    let currentZoomLevel = parseInt(zoomView.style.backgroundSize.split(' ')[0]) || config.default;
    
    if (deltaY < 0 && currentZoomLevel > (config.min + config.step)) {
        zoomView.style.backgroundSize = currentZoomLevel - 10 + '%';
        // zoomer.style.setProperty('--zoom-size', currentZoomLevel - 10 + '%')
    } else if (deltaY > 0 && currentZoomLevel <= config.max) {
        zoomView.style.backgroundSize = currentZoomLevel + 10 + '%';
        // zoomer.style.setProperty('--zoom-size', currentZoomLevel + 10 + '%')
    }
}